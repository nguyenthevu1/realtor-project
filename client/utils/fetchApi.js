import axios from 'axios';

export const baseUrl = 'https://bayut.p.rapidapi.com';

export const fetchApi = async (url) => {
    const { data } = await axios.get(url, {
        headers: {
            'X-RapidAPI-Key': '67e3cf8ea8msh786b496a70ac1afp12ab35jsnec9a8562e5df',
            'X-RapidAPI-Host': 'bayut.p.rapidapi.com',
        },
    });

    return data;
};
